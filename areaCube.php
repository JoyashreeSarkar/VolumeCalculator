<?php
use \Pondit\Calculator\VolumeCalculator\Cube;
use \Pondit\Calculator\VolumeCalculator\Displayer;

include_once "vendor/autoload.php";


$cube1 = new Cube();
$cube1->radius = 2;



$cube2 = new Cube();
$cube2->radius = 2;

$displayer1 = new Displayer();

$displayer1->displaypre($cube1->getArea());
$displayer1->displayh1($cube2->getArea());
