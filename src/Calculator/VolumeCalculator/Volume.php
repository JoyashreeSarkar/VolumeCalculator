<?php
namespace Pondit\Calculator\VolumeCalculator;


class Volume
{
    public $width;
    public $length;
    public $height;

    public function getArea()
    {
        return $this->length * $this->width * $this->height;
    }

}