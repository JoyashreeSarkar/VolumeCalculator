<?php
/**
 * Created by PhpStorm.
 * User: Joya
 * Date: 3/22/2018
 * Time: 12:12 AM
 */

namespace Pondit\Calculator\VolumeCalculator;


class Cube
{
    public $radius;

    public function getArea()
    {
        return 6*$this->radius*$this->radius;
    }


}