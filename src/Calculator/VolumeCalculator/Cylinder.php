<?php
/**
 * Created by PhpStorm.
 * User: Joya
 * Date: 3/22/2018
 * Time: 12:03 AM
 */

namespace Pondit\Calculator\VolumeCalculator;


class Cylinder
{
    public $pi=3.1416;
    public $radius;
    public $height;

    public function getArea()
    {
        return (2*$this->pi * $this->radius * $this->height)+(2*$this->pi * $this->radius * $this->radius);
    }

}