<?php
use \Pondit\Calculator\VolumeCalculator\Volume;
use \Pondit\Calculator\VolumeCalculator\Displayer;

include_once "vendor/autoload.php";


$volume1 = new Volume();
$volume1->width = 10;
$volume1->length = 20;
$volume1->height = 5;


$volume2 = new Volume();
$volume2->width = 3;
$volume2->length = 3;
$volume2->height = 3;

$displayer1 = new Displayer();

$displayer1->displaypre($volume1->getArea());
$displayer1->displayh1($volume2->getArea());
