<?php
use \Pondit\Calculator\VolumeCalculator\Cylinder;
use \Pondit\Calculator\VolumeCalculator\Displayer;

include_once "vendor/autoload.php";


$cylinder1 = new Cylinder();
$cylinder1->radius = 2;
$cylinder1->height = 5;


$cylinder2 = new Cylinder();
$cylinder2->radius = 3;
$cylinder2->height = 3;

$displayer1 = new Displayer();

$displayer1->displaypre($cylinder1->getArea());
$displayer1->displayh1($cylinder2->getArea());
